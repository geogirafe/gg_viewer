import SearchComponent from '../search/component';

class MobileSearchComponent extends SearchComponent {
  templateUrl = './template.html';
  styleUrls = ['../../styles/common.css', './style.css'];
}

export default MobileSearchComponent;
